vmexec
======

vmexec boots a given virtual machine image and wait until it comes up. Then it
runs a list of given commands one after the other on the guest, aborting on
receiving an error.  When all commands are run (or one of them failed), the
virtual machine is shut down and stopped.

Commands are by default run inside the virtual machine using ssh(1) on UNIX and
telnet(1) on Windows. By prefixing a command with an equals sign '=', it will
instead be run on the host system (for example to copy files into or out of the
virtual machine using scp(1)).

Some care is taken to ensure that the virtual machine is shutdown gracefully
and not left running even in case the controlling tty is closed or the parent
process killed. If a previous virtual machine is already running on a
conflicting port, an attempt is made to shut it down first. For this purpose, a
PID file is created in $HOME/.vmexec/
